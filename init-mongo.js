db.auth('admin', 'F$2$umKfgwaAwH#YuUKAxx#*LQ9yqe5QoTmWS#WnfWG&gDKp!RaX#MntZTACNs#8AvCqp#')

db = db.getSiblingDB('IDP_DB')

db.createUser({
  user: 'admin',
  pwd: 'F$2$umKfgwaAwH#YuUKAxx#*LQ9yqe5QoTmWS#WnfWG&gDKp!RaX#MntZTACNs#8AvCqp#',
  roles: [
    {
      role: 'root',
      db: 'admin',
    },
  ],
});

db.createUser({
  user: 'spike',
  pwd: '#ue&eQAJ@2u$3GiqFQ9TBn6C*htb5cVEK^PWNcpd@SxmYeK#7!^5!5qMnwWjoRQJMoA4bV',
  roles: [
    {
      role: 'readWrite',
      db: 'IDP_DB',
    },
  ],
});