FROM tiangolo/uvicorn-gunicorn-fastapi:latest

COPY requirements.txt .

# Install the dependencies
RUN pip install --no-cache-dir  -r requirements.txt
# Open API validator
RUN pip install --no-cache-dir  email-validator

COPY ./app /app

# Set the working directory
WORKDIR /app