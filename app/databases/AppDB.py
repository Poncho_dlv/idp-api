#!/usr/bin/env python
# -*- coding: utf-8 -*-

from enum import IntEnum
from databases.BaseMongoDb import BaseMongoDb
from settings.Settings import Settings
from bson.objectid import ObjectId


collection_name = "app"


class AppStatus(IntEnum):
    ACTIVE = 1
    DISABLED = 2


class AppDB(BaseMongoDb):
    def __init__(self):
        super(AppDB, self).__init__(Settings.get_database_user(),
                                    Settings.get_database_password(),
                                    Settings.get_database_host(),
                                    Settings.get_database_name())

    def create_app(self, app_name: str, owner_id: str, client_secret: str):
        self.open_database()
        collection = self.db[collection_name]
        user_data = {
            "app_name": app_name,
            "owner_id": ObjectId(owner_id),
            "client_secret": client_secret,
            "status": AppStatus.ACTIVE,
            "scopes": [""]
        }
        ret = collection.insert_one(user_data)
        self.close_database()
        return ret

    def delete_app(self, app_id: str,  owner_id: str):
        self.open_database()
        collection = self.db[collection_name]
        ret = collection.delete_one({"_id": ObjectId(app_id), "owner_id": ObjectId(owner_id)})
        self.close_database()
        return ret

    def get_app_data(self, client_id: str, client_secret: str):
        self.open_database()
        collection = self.db[collection_name]
        ret = collection.find_one({"_id": ObjectId(client_id), "client_secret": client_secret})
        self.close_database()
        return ret

    def update_token(self, client_id: str, token: str):
        self.open_database()
        collection = self.db[collection_name]
        query = {"_id": ObjectId(client_id)}
        new_value = {
            "$set": {
                "token": token
            }
        }
        collection.update_one(query, new_value, upsert=False)
        self.close_database()
