#!/usr/bin/env python
# -*- coding: utf-8 -*-


class InvalidCredential(Exception):
    def __init__(self):
        self.message = "Invalid  credential."


class InvalidServer(Exception):
    def __init__(self):
        self.message = "Invalid  server address."


class InvalidDatabase(Exception):
    def __init__(self):
        self.message = "Invalid  database."
