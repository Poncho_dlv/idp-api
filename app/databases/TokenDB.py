#!/usr/bin/env python
# -*- coding: utf-8 -*-

from enum import IntEnum
from databases.BaseMongoDb import BaseMongoDb
from settings.Settings import Settings
from bson.objectid import ObjectId


collection_name = "token"


class TokenType(IntEnum):
    USER_TOKEN = 1
    APP_TOKEN = 2


class TokenDB(BaseMongoDb):
    def __init__(self):
        super(TokenDB, self).__init__(Settings.get_database_user(),
                                      Settings.get_database_password(),
                                      Settings.get_database_host(),
                                      Settings.get_database_port(),
                                      Settings.get_database_name())

    def update_token(self, subject_id: str, token: str):
        self.open_database()
        collection = self.db[collection_name]
        query = {"subject_id": ObjectId(subject_id)}
        new_value = {
            "$set": {
                "subject_id": ObjectId(subject_id),
                "token": token
            }
        }
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def token_is_valid(self, subject_id: str, token: str):
        self.open_database()
        collection = self.db[collection_name]
        ret = collection.find_one({"subject_id": ObjectId(subject_id)}, {"_id": 0, "token": 1})
        self.close_database()
        return ret is not None and ret.get("token") == token

    def delete_token(self, subject_id: str):
        self.open_database()
        collection = self.db[collection_name]
        ret = collection.delete_one({"subject_id": ObjectId(subject_id)})
        self.close_database()
        return ret
