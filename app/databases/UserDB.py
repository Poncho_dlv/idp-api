#!/usr/bin/env python
# -*- coding: utf-8 -*-

from enum import IntEnum

from bson.objectid import ObjectId
from databases.BaseMongoDb import BaseMongoDb
from settings.Settings import Settings

collection_name = "users"


class AccountStatus(IntEnum):
    VALIDATION_PENDING = 0
    ACTIVE = 1
    DISABLED = 2


class UsersDB(BaseMongoDb):
    def __init__(self):
        super(UsersDB, self).__init__(Settings.get_database_user(),
                                      Settings.get_database_password(),
                                      Settings.get_database_host(),
                                      Settings.get_database_port(),
                                      Settings.get_database_name())

    def add_user(self, username: str, hashed_password: str, email: str, full_name: str):
        self.open_database()
        collection = self.db[collection_name]
        user_data = {
            "username": username,
            "hashed_password": hashed_password,
            "email": email,
            "full_name": full_name,
            "status": AccountStatus.VALIDATION_PENDING,
            "scopes": ["me"]
        }
        ret = collection.insert_one(user_data)
        self.close_database()
        return ret

    def user_exist(self, username: str = None, user_id: str = None):
        self.open_database()
        collection = self.db[collection_name]
        query = None
        if username is not None:
            query = {"username": username}
        elif user_id is not None:
            query = {"_id": ObjectId(user_id)}

        if query is None:
            return False

        ret = collection.find_one(query, {"_id": 1})
        self.close_database()
        return ret is not None

    def get_user_data(self, username: str, ignore_password: bool = True):
        self.open_database()
        collection = self.db[collection_name]
        if ignore_password:
            request_filter = {"hashed_password": 0}
        else:
            request_filter = None
        ret = collection.find_one({"username": username}, request_filter)
        ret["_id"] = str(ret["_id"])
        self.close_database()
        return ret

    def get_user(self, user_id: str):
        self.open_database()
        collection = self.db[collection_name]
        request_filter = {"hashed_password": 0}
        ret = collection.find_one({"_id": user_id}, request_filter)
        ret["_id"] = str(ret["_id"])
        self.close_database()
        return ret

    def delete_user(self, user_id: str):
        self.open_database()
        collection = self.db[collection_name]
        ret = collection.delete_one({"_id": ObjectId(user_id)})
        self.close_database()
        return ret
