#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pymongo
from databases.Exception import InvalidCredential, InvalidServer, InvalidDatabase
import urllib.parse


class BaseMongoDb:

    def __init__(self, db_user: str = None, db_password: str = None, db_host: str = None, db_port: int = None, db_name: str = None):
        self.client = None
        self.db = None
        self.db_user = db_user
        self.db_password = db_password
        self.db_host = db_host
        self.db_port = db_port
        self.db_name = db_name

    def open_database(self):
        if self.client is None or self.db is None:
            if self.db_host is None:
                raise InvalidServer

            if self.db_user is None or self.db_password is None:
                raise InvalidCredential

            if self.db_name is None:
                raise InvalidDatabase
            enc_password = urllib.parse.quote_plus(self.db_password)
            mongo_uri = f"mongodb://{self.db_user}:{enc_password}@{self.db_host}:{self.db_port}/{self.db_name}"
            self.client = pymongo.MongoClient(mongo_uri, connect=False)
            self.db = self.client[self.db_name]

    def close_database(self):
        if self.client is not None:
            self.client.close()
            self.client = None
            self.db = None

