import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routers import User, App

app = FastAPI(title="IDP API",
              description="Manage identity of users and application",
              version="1.0.0",
              contact={
                  "name": "Poncho",
                  "email": "poncho_dlv@pm.me"
              },
              redoc_url=None,
              )

origins = [
    "http://localhost",
    "http://localhost:8080",
    "https://spike.ovh",
    "https://*.spike.ovh",
    "spike_api"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(User.users)
app.include_router(App.app)

if __name__ == "__main__":
    uvicorn.run(app)
