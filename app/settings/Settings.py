import json
import os


class Settings:

    def __init__(self):
        self.json_data = Settings.load_json()

    @staticmethod
    def load_json():
        filename = os.environ.get("SETTINGS_FILE", "settings/settings.json")
        try:
            with open(filename, encoding="utf-8") as f:
                json_data = json.load(f)
            f.close()
        except FileNotFoundError:
            json_data = {}
        return json_data

    def save_settings(self):
        filename = os.environ.get("SETTINGS_FILE", "settings/settings.json")
        with open(filename, 'w') as outfile:
            json.dump(self.json_data, outfile)
        outfile.close()

    def check_or_create_section(self, section):
        if self.json_data.get(section) is None:
            self.json_data[section] = {}

    @staticmethod
    def get_secret_id():
        json_data = Settings.load_json()
        return json_data.get("secretId")

    # Token
    @staticmethod
    def get_token_secret_key():
        json_data = Settings.load_json()
        return json_data.get("token", {}).get("secretKey")

    @staticmethod
    def get_token_algorithm():
        json_data = Settings.load_json()
        return json_data.get("token", {}).get("Algorithm")

    @staticmethod
    def get_token_expiration():
        json_data = Settings.load_json()
        return json_data.get("token", {}).get("tokenExpiration")

    # Database
    @staticmethod
    def get_database_host():
        json_data = Settings.load_json()
        return json_data.get("MongoDb", {}).get("host")

    @staticmethod
    def get_database_port():
        json_data = Settings.load_json()
        return int(json_data.get("MongoDb", {}).get("port", 0))

    @staticmethod
    def get_database_user():
        json_data = Settings.load_json()
        return json_data.get("MongoDb", {}).get("user")

    @staticmethod
    def get_database_password():
        json_data = Settings.load_json()
        return json_data.get("MongoDb", {}).get("password")

    @staticmethod
    def get_database_name():
        json_data = Settings.load_json()
        return json_data.get("MongoDb", {}).get("name")
