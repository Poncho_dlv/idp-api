from fastapi import APIRouter, HTTPException, Body, status, Header
import re
from datetime import timedelta
from passlib.context import CryptContext
from models import User, Token
from databases.UserDB import UsersDB
from databases.TokenDB import TokenDB, TokenType
from settings.Settings import Settings
from utilities.Authentication import create_access_token, check_token

users = APIRouter()

# for validating an Email
mail_regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

# for validating username
username_regex = r'[=-@_!#$%^&*()<>?/\|\'\"}{~:]'


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
router_name = "Users"


def get_user(username: str):
    db = UsersDB()
    if db.user_exist(username):
        user_dict = db.get_user_data(username, False)
        return User.UserInDB(**user_dict)
    return False


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def authenticate_user(username: str, password: str):
    user = get_user(username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


@users.post("/sign_in/", tags=[router_name])
async def sign_in_user(new_user: User.UserSignIn = Body(default=None)):

    if len(new_user.username) == 0 or len(re.findall(username_regex, new_user.username)) > 0:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid username")

    if len(new_user.full_name) == 0 or len(re.findall(username_regex, new_user.full_name)) > 0:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid full_name")

    if len(new_user.email) < 5 or re.fullmatch(mail_regex, new_user.email) is None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="invalid email address")

    if len(new_user.password) < 8:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid password")

    db = UsersDB()
    if not db.user_exist(new_user.username):
        hashed_password = get_password_hash(new_user.password)
        user_ret = db.add_user(new_user.username, hashed_password, new_user.email, new_user.full_name)
        if user_ret is not None:
            return {"user_id": str(user_ret.inserted_id)}
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Unable to create user")
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Already exist.")


@users.delete("/users/{user_id}/remove", tags=[router_name])
async def remove_user(user_id: str, token: str = Header(default=None)):
    token_data = check_token(token, "me")

    if user_id != token_data.get("sub"):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Unauthorized operation.")

    user_db = UsersDB()
    token_db = TokenDB()

    if not user_db.user_exist(user_id=user_id):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found.")
    user_db.delete_user(user_id)
    token_db.delete_token(token_data.get("sub"))
    return {"detail": "Deleted successfully."}


@users.post("/login/", response_model=Token.Token, tags=[router_name])
async def login_user(form_data: User.UserLogin = Body(default=None)):

    user = authenticate_user(form_data.username, form_data.password)

    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found.")

    for scope in form_data.scopes:
        if scope not in user.scopes:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=f"Unauthorized scope: {scope}")

    if not user:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Incorrect username or password")

    token_data = {
        "sub": user.user_id,
        "scopes": form_data.scopes,
        "type": int(TokenType.USER_TOKEN)
    }

    access_token_expires = timedelta(minutes=Settings.get_token_expiration())
    access_token = create_access_token(data=token_data, expires_delta=access_token_expires)
    return {"access_token": access_token, "token_type": "bearer"}


@users.get("/users/{user_id}/verify_token", tags=[router_name])
def verify_token(user_id: str, scope: str = Header(None), token: str = Header(None)):
    payload = check_token(token, scope)
    if payload.get("sub") != user_id:
        raise HTTPException(status_code=status.HTTP_406_NOT_ACCEPTABLE, detail="Inconsistent data user and token")
    return True
