from fastapi import APIRouter, HTTPException, Header, Body, status
from models import Token, App
from databases.AppDB import AppDB
from databases.TokenDB import TokenType
from settings.Settings import Settings
from datetime import timedelta
import secrets
from utilities.Authentication import check_token, create_access_token

app = APIRouter()
router_name = "Applications"


def get_app(client_id: str, client_secret: str):
    db = AppDB()
    app_dict = db.get_app_data(client_id, client_secret)
    return app_dict


def authenticate_app(client_id: str, client_secret: str):
    auth_app = get_app(client_id, client_secret)
    if not auth_app:
        return None
    return auth_app


@app.post("/apps/create", tags=[router_name])
async def create_app(new_app: App.AppCreate, token: str = Header(default=None)):
    token_data = check_token(token, "app.creation")

    app_db = AppDB()
    client_secret = secrets.token_hex(16)
    app_ret = app_db.create_app(new_app.name, token_data.get("sub"), client_secret)

    if app_ret is not None:
        return {"client_id": str(app_ret.inserted_id), "client_secret": client_secret}

    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Unable to create app")


@app.delete("/apps/{application_id}/remove", tags=[router_name])
async def remove_app(application_id: str, token: str = Header(default=None)):
    token_data = check_token(token, "app.remove")
    app_db = AppDB()
    app_db.delete_app(application_id, token_data.get("sub"))
    return {"detail": "Deleted successfully."}


@app.post("/apps/{application_id}/token", response_model=Token.Token, tags=[router_name])
async def login_app(log_app: App.AppLogin = Body(default=None)):
    auth_app = authenticate_app(log_app.client_id, log_app.client_secret)

    if not auth_app:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid credential")

    for scope in log_app.scopes:
        if scope not in auth_app.scopes:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=f"Unauthorized scope: {scope}")

    access_token_expires = timedelta(minutes=Settings.get_token_expiration())
    token_data = {
        "sub": log_app.client_id,
        "scopes": log_app.scopes,
        "type": TokenType.APP_TOKEN
    }

    access_token = create_access_token(data=token_data, expires_delta=access_token_expires)
    return {"access_token": access_token, "token_type": "bearer"}


@app.get("/apps/{application_id}/verify_token", tags=[router_name])
def verify_token(application_id: str, scope: str = Header(None), token: str = Header(None)):
    payload = check_token(token, scope)
    if payload.get("sub") != application_id:
        raise HTTPException(status_code=status.HTTP_406_NOT_ACCEPTABLE, detail="Inconsistent data application and token")
    return True
