from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

user_data = {
    "username": "Bob 2",
    "user_id": "63133b2f09d4faabdff17a8d",
    "access_token": None,
    "password": "azertyup",
    "scopes": ["me", "app.creation", "app.remove"]
}


def test_get_user_token():
    body = {
        "username": "Bob 2",
        "password": "azertyup",
        "scopes": ["me", "app.creation", "app.remove"]
    }
    response = client.post("/login/", json=body)
    user_data["access_token"] = response.json()["access_token"]


def test_create_app_no_token():
    response = client.post("/apps/create", json={})
    error_list = response.json()["detail"]
    assert response.status_code == 401
    assert response.json()["detail"] == "Invalid token."


def test_create_app_no_body():
    response = client.post("/apps/create", json={}, headers={"token": user_data["access_token"]})
    error_list = response.json()["detail"]
    assert response.status_code == 422
    assert response.json()["detail"] == "Invalid token."
