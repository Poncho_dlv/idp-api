from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

user_id = None
token = None


def test_create_user_empty_body():
    response = client.post("/sign_in/", json={})
    error_list = response.json()["detail"]
    assert response.status_code == 422
    assert len(error_list) == 4
    for error in error_list:
        assert error["msg"] == "field required"


def test_create_user_fields_required():
    body = {
        "username": "",
        "full_name": "",
        "email": "",
        "password": ""
    }
    response = client.post("/sign_in/", json=body)
    assert response.status_code == 400
    assert response.json()["detail"] == "Invalid username"

    body = {
        "username": "Bob",
        "full_name": "",
        "email": "",
        "password": ""
    }
    response = client.post("/sign_in/", json=body)
    assert response.status_code == 400
    assert response.json()["detail"] == "Invalid full_name"

    body = {
        "username": "Bob",
        "full_name": "Bob test",
        "email": "",
        "password": ""
    }
    response = client.post("/sign_in/", json=body)
    assert response.status_code == 400
    assert response.json()["detail"] == "invalid email address"

    body = {
        "username": "Bob",
        "full_name": "Bob test",
        "email": "@.",
        "password": ""
    }
    response = client.post("/sign_in/", json=body)
    assert response.status_code == 400
    assert response.json()["detail"] == "invalid email address"

    body = {
        "username": "Bob",
        "full_name": "Bob test",
        "email": "bob@bob.com",
        "password": ""
    }
    response = client.post("/sign_in/", json=body)
    assert response.status_code == 400
    assert response.json()["detail"] == "Invalid password"

    body = {
        "username": "Bob",
        "full_name": "Bob test",
        "email": "bob@bob.com",
        "password": "azertyu"
    }
    response = client.post("/sign_in/", json=body)
    assert response.status_code == 400
    assert response.json()["detail"] == "Invalid password"


def test_create_user():
    body = {
        "username": "Bob",
        "full_name": "Bob test",
        "email": "bob@bob.com",
        "password": "azertyup",
        "scopes": ["me"]
    }

    # Create user
    response = client.post("/sign_in/", json=body)
    assert response.status_code == 200
    global user_id
    user_id = response.json()["user_id"]


def test_create_user_already_exist():
    body = {
        "username": "Bob",
        "full_name": "Bob test",
        "email": "bob@bob.com",
        "password": "azertyup",
        "scopes": ["me"]
    }

    response = client.post("/sign_in/", json=body)
    assert response.status_code == 400
    assert response.json()["detail"] == "Already exist."


def test_login():
    body = {
        "username": "Bob",
        "password": "azertyup",
        "scopes": ["me"]
    }
    response = client.post("/login/", json=body)
    assert response.status_code == 200
    assert response.json()["token_type"] == "bearer"
    global token
    token = response.json()["access_token"]


def test_remove_fake_user():
    response = client.delete("/users/FAKE_ID/remove", headers={"token": token})
    assert response.status_code == 401
    assert response.json()["detail"] == "Unauthorized operation."


def test_remove_user():
    response = client.delete("/users/{}/remove".format(user_id), headers={"token": token})
    assert response.status_code == 200
    assert response.json()["detail"] == "Deleted successfully."


def test_remove_unknown_user():
    response = client.delete("/users/{}/remove".format(user_id), headers={"token": token})
    assert response.status_code == 404
    assert response.json()["detail"] == "Invalid token."

# TODO test token expiration
