from pydantic import BaseModel
from fastapi import Body
from typing import Optional


class AppAuth(BaseModel):
    client_id: str
    client_secret: str


class AppLogin(AppAuth):
    scopes: Optional[list[str]] = []


class AppCreate(BaseModel):
    name: str = Body(default=None)

