from typing import Optional
from pydantic import BaseModel, Field
from databases.UserDB import AccountStatus


class UserBase(BaseModel):
    username: str
    password: str


class UserLogin(UserBase):
    scopes: Optional[list[str]] = ["me"]


class UserInDB(BaseModel):
    user_id: str = Field(None, alias="_id")
    hashed_password: str
    username: str
    email: Optional[str] = None
    full_name: Optional[str] = None
    status: Optional[AccountStatus] = AccountStatus.VALIDATION_PENDING
    scopes: Optional[list[str]] = ["me"]


class UserSignIn(UserBase):
    email: str
    full_name: str
