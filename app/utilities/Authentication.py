from datetime import datetime, timedelta
from typing import Optional
from fastapi import HTTPException, status
from jose import JWTError, jwt
from databases.TokenDB import TokenDB
from settings.Settings import Settings


def check_token(token: str, scope_required: str):
    if token is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid token.")

    try:
        db = TokenDB()
        utc_now = datetime.utcnow()
        payload = jwt.decode(token, Settings.get_token_secret_key(), algorithms=[Settings.get_token_algorithm()])
        expiration = datetime.utcfromtimestamp(payload.get("exp"))

        scopes = payload.get("scopes", [])
        if scope_required is not None and scope_required not in scopes:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid scope.")

        if expiration < utc_now:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Token expired.")

        if not db.token_is_valid(payload.get("sub"), token):
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Invalid token.")

        return payload

    except JWTError as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=e)


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()

    # Add common data
    to_encode.update({"iss": Settings.get_secret_id()})

    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, Settings.get_token_secret_key(), algorithm=Settings.get_token_algorithm())
    token_db = TokenDB()
    token_db.update_token(to_encode.get("sub"), encoded_jwt)
    return encoded_jwt
