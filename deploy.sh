#!/bin/bash

git clone git@bitbucket.org:Poncho_dlv/idp-api.git

# shellcheck disable=SC2164
cd idp-api

docker-compose up --force-recreate --build -d

# shellcheck disable=SC2103
cd ..

rm -R idp-api/ -f